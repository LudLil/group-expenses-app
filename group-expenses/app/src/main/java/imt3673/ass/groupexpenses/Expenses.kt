package imt3673.ass.groupexpenses

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

// Represents all the expenses of the group of people.
@Parcelize
class Expenses : Parcelable {

    @IgnoredOnParcel
    private val expensesList = mutableListOf<SingleExpense>()
    @IgnoredOnParcel
    private var exists = false

    // Adds new expense to the expenses list.
    // If the Person does not exist in the expenses,
    // the person is added to the list, and false is returned.
    // If the Person already exist in the expenses,
    // the new expense amount is added to the person's existing amount and true is returned.
    // There should only be one instance of SingleExpense associated with a single person.
    fun add(expense: SingleExpense): Boolean {
        exists = false
        var index = -1
        var amount: Long
        var newExpense = expense

        if (expensesList.isNotEmpty()) {
            expensesList.forEach {
                index++
                if (it.person == expense.person) {
                    exists = true
                    amount = it.amount
                    amount += expense.amount
                    newExpense = SingleExpense(expense.person, amount, expense.description)
                }
            }
        }

        if (exists) {
            expensesList[index] = newExpense
        } else {
            expensesList.add(expense)
        }

        return exists
    }

    // Replaces the expense for a given person
    // This method is similar to #addExpense, however, instead of adding
    // the claim amount to the existing person, it replaces it instead.
    fun replace(expense: SingleExpense): Boolean {
        exists = false
        var index = -1
        expensesList.forEach {
            if (it.person == expense.person) {
                exists = true
            }
            index++
        }

        if (exists) {
            expensesList[index] = expense
        } else {
            expensesList.add(expense)
        }

        return exists
    }

    // Removes an expense association for this person.
    // If the person exists in the expenses, it returns true.
    // Otherwise, it returns false.
    fun remove(person: String): Boolean {
        exists = false
        var index = -1
        expensesList.forEach {
            if (it.person == person) {
                exists = true
            }
            index++
        }

        if (exists) {
            expensesList.removeAt(index)
        }

        return exists
    }

    // Returns the amount of expenses for a given person.
    // If the person does not exist, the function returns failed result.
    fun amountFor(person: String): Result<Long> {
        var amount: Long
        expensesList.forEach {
            if (it.person == person) {
                amount = it.amount
                return Result.success(amount)
            }
        }
        return Result.failure(Throwable("A person with this name does not exist"))
    }

    // Returns the list of all expenses.
    // If no expenses have been added yet, the function returns an empty list.
    fun allExpenses(): List<SingleExpense> {
        if (expensesList.isNotEmpty()) {
            return expensesList
        }
        return mutableListOf()
    }

    // Makes a deep copy of this expense instance
    fun copy(): Expenses {
        val exp = Expenses()
        allExpenses().forEach {
            exp.add(SingleExpense(it.person, it.amount, it.description))
        }
        return exp
    }
}