package imt3673.ass.groupexpenses

import java.io.Serializable

// Represents a single expense.
class SingleExpense(val person: String,
                    val amount: Long,
                    var description: String = "") : Serializable {

    override fun toString(): String {
        return "Expense{$person, $amount}"
    }
}
