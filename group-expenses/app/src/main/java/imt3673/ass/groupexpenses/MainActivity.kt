package imt3673.ass.groupexpenses

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.setPadding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    // The storage for all expenses
    var expenses: Expenses = Expenses()
    private var settlement = listOf<Transaction>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupUI()
        updateTable()
    }

    // No data is lost on screen rotation
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        // Saves expenses
        outState.putParcelable("expenses", expenses)
        tbl_expenses.removeAllViews()
    }
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        // Restores expenses and all of its single expenses
        savedInstanceState.let { bundle ->
            val restoredExpenses = bundle.getParcelable<Expenses>("expenses")!!
            restoredExpenses.allExpenses().forEach {
                expenses.add(it)
            }
            tbl_expenses.removeAllViews()
            updateTable()

            if (expenses.allExpenses().isNotEmpty()) {
                updateSumAvg()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        setupUI()

        // To prevent table from disappearing when you exit the app and open it again
        if (tbl_expenses.childCount == 0) {
            updateTable()
        }
    }

    // Implements the settlement calculation and keeps it in this.settlement
    private fun updateSettlement() {
        this.settlement = calculateSettlement(this.expenses)
    }

    private fun setupUI() {
        btn_add_data.setOnClickListener {
            val intent = Intent(this, DataEntry::class.java)

            // The MainActivity view checks if DataEntry is finished and if it returns data
            startActivityForResult(intent, 1)
        }

        if (expenses.allExpenses().size > 1) {
            btn_settlement.isEnabled = true
            btn_settlement.setBackgroundColor(Color.parseColor("#FF4CAF50"))

            btn_settlement.setOnClickListener {
                val intent = Intent(this, Settlement::class.java)
                updateSettlement()
                val settlementArrList = ArrayList(settlement)
                intent.putParcelableArrayListExtra("Settlement", settlementArrList)
                startActivityForResult(intent, 2)
            }
        }
    }

    // Updates the sum and average of all expenses
    private fun updateSumAvg() {
        var numOfExpenses: Long = 0
        var amountSum: Long = 0
        val amountAvg: Long

        expenses.allExpenses().forEach {
            numOfExpenses++
            amountSum += it.amount
        }

        amountAvg = amountSum / numOfExpenses

        txt_expenses_total.text = convertAmountToString(amountSum)
        txt_expenses_avr.text = convertAmountToString(amountAvg)
    }

    // Updates table when DataEntry activity is done
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                val expense = data?.getSerializableExtra("Expense") as SingleExpense
                expenses.add(expense)
                tbl_expenses.removeAllViews()
                updateTable()
                updateSumAvg()
            } else {
                updateTable()
                return
            }
        } else {
            return
        }
    }

    // Updates table with all expenses
    private fun updateTable() {
        var row: TableRow
        var nameText: TextView
        var amountText: TextView
        var descText: TextView
        val expensesList = expenses.allExpenses()

        // Header row
        row = TableRow(this)
        row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        nameText = TextView(this)
        nameText.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.toFloat())
        nameText.gravity = Gravity.CENTER
        nameText.setPadding(10)
        nameText.text = getString(R.string.nameText)
        nameText.textSize = 24.toFloat()
        row.addView(nameText)

        amountText = TextView(this)
        amountText.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.toFloat())
        amountText.gravity = Gravity.CENTER
        amountText.setPadding(10)
        amountText.text = getString(R.string.amtText)
        amountText.textSize = 24.toFloat()
        row.addView(amountText)

        descText = TextView(this)
        descText.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.toFloat())
        descText.gravity = Gravity.CENTER
        descText.setPadding(10)
        descText.text = getString(R.string.descText)
        descText.textSize = 24.toFloat()
        row.addView(descText)

        tbl_expenses.addView(row)

        // Adds rows for all expenses if the list of expenses is not empty
        if (expensesList.isNotEmpty()) {
            for (i in expensesList) {
                row = TableRow(this)
                row.layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )

                nameText = TextView(this)
                nameText.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT,
                    1.toFloat()
                )
                nameText.gravity = Gravity.CENTER
                nameText.setPadding(10)
                nameText.text = i.person
                nameText.textSize = 24.toFloat()
                row.addView(nameText)

                amountText = TextView(this)
                amountText.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT,
                    1.toFloat()
                )
                amountText.gravity = Gravity.CENTER
                amountText.setPadding(10)
                amountText.text = convertAmountToString(i.amount)
                amountText.textSize = 24.toFloat()
                row.addView(amountText)

                descText = TextView(this)
                descText.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT,
                    1.toFloat()
                )
                descText.gravity = Gravity.CENTER
                descText.setPadding(10)
                descText.text = i.description
                descText.textSize = 24.toFloat()
                row.addView(descText)

                tbl_expenses.addView(row)
            }
        }
    }
}
