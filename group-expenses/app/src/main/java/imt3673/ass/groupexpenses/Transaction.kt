package imt3673.ass.groupexpenses

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Represents a single settlement transaction.
 * Payment is from the payer to payee.
 * The source and destination here are names of respective people.
 */
@Parcelize
class Transaction(val payer: String,
                  val payee: String,
                  val amount: Long) : Parcelable {

    override fun toString(): String {
        return "Tx{$payer, $payee, $amount}"
    }
}

