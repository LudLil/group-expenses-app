package imt3673.ass.groupexpenses

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.data_entry.*


class DataEntry : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.data_entry)

        var sanitizedName: String
        var result: Result<Long>
        var exception: String     // Exception message from Result

        // Function to hide the keyboard
        fun View.hideKeyboard() {
            val inputMethodManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            inputMethodManager?.hideSoftInputFromWindow(this.windowToken, 0)
        }

        // Keyboard is hidden when user touches outside of it
        dataentry_view.setOnClickListener {
            it.hideKeyboard()
        }

        // Sanitizes name when the name input field is not in focus
        edit_person.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                sanitizedName = sanitizeName(edit_person.text.toString())
                edit_person.setText(sanitizedName)
            }
        }

        edit_person.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                sanitizedName = sanitizeName(edit_person.text.toString())
                edit_person.setText(sanitizedName)
            }
            false
        }

        // TextWatcher object that displays error messages when any input field has invalid data,
        // enables "Add" button when all fields are valid
        val watcher: TextWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {

                // If input field is for name
                if (s.hashCode() == edit_person.text.hashCode()) {
                    val name = s.toString()

                    // Empty name
                    if (name.trim().replace("\\s+".toRegex(), " ").isEmpty()) {
                        txt_add_expenses_error.text = getString(R.string.emptyName)
                    }

                    // Name has invalid characters
                    else if (!name.matches("[æøåÆØÅa-zA-Z -]+".toRegex())) {
                        txt_add_expenses_error.text = getString(R.string.errorName)
                    }

                    // Removes error message, checks if itself and all other fields are out of
                    // focus and have valid data.
                    else {
                        txt_add_expenses_error.text = ""
                    }
                }

                // If input field is for amount
                else if (s.hashCode() == edit_amount.text.hashCode()) {

                    // Tries to convert string to long amount if the field is out of focus
                    edit_amount.setOnFocusChangeListener { _, hasFocus ->
                        if (!hasFocus) {
                            result = convertStringToAmount(edit_amount.text.toString())

                            // Adds Result exception message to the error message text
                            if (result.isFailure) {
                                exception = result.exceptionOrNull().toString().substring(21)
                                txt_add_expenses_error.text = getString(R.string.amountError, exception)
                            }

                            else {
                                txt_add_expenses_error.text = ""
                            }
                        }
                    }

                    edit_amount.setOnEditorActionListener { _, actionId, _ ->
                        if (actionId == EditorInfo.IME_ACTION_NEXT) {
                            result = convertStringToAmount(edit_amount.text.toString())

                            // Adds Result exception message to the error message text
                            if (result.isFailure) {
                                exception = result.exceptionOrNull().toString().substring(21)
                                txt_add_expenses_error.text = getString(R.string.amountError, exception)
                            }

                            else {
                                txt_add_expenses_error.text = ""
                            }
                        }
                        false
                    }
                }

                // If input field is for description
                else if (s.hashCode() == edit_description.text.hashCode()) {

                    edit_description.setOnFocusChangeListener { _, hasFocus ->
                        if (!hasFocus) {
                            val desc = s.toString()

                            // Empty description
                            if (desc.trim().replace("\\s+".toRegex(), " ").isEmpty()) {
                                txt_add_expenses_error.text = getString(R.string.noDesc)
                            }

                            // Does the same as for the other fields
                            else {
                                txt_add_expenses_error.text = ""
                            }
                        }
                    }

                    edit_description.setOnEditorActionListener { _, actionId, _ ->
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            val desc = s.toString()

                            // Empty description
                            if (desc.trim().replace("\\s+".toRegex(), " ").isEmpty()) {
                                txt_add_expenses_error.text = getString(R.string.noDesc)
                            }

                            // Does the same as for the other fields
                            else {
                                txt_add_expenses_error.text = ""
                            }
                        }
                        false
                    }
                }

                if (edit_person.text.isNotEmpty() && edit_amount.text.isNotEmpty() &&
                    edit_description.text.isNotEmpty()
                    && txt_add_expenses_error.text.isEmpty()) {
                    btn_add_expense.isEnabled = true
                    btn_add_expense.setBackgroundColor(Color.parseColor("#FF4CAF50"))
                } else {
                    btn_add_expense.isEnabled = false
                    btn_add_expense.setBackgroundColor(Color.parseColor("#D7D7D7"))
                }
            }
        }

        // Adds TextWatcher listener for all fields
        edit_person.addTextChangedListener(watcher)
        edit_amount.addTextChangedListener(watcher)
        edit_description.addTextChangedListener(watcher)

        // When "Add" button is clicked, make new single expense and
        // pass it back to MainActivity
        btn_add_expense.setOnClickListener {
            val name = edit_person.text.toString()
            result = convertStringToAmount(edit_amount.text.toString())
            val amount = result.getOrNull()
            val description = edit_description.text.toString()

            if (amount != null) {
                val expense = SingleExpense(name, amount, description)
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("Expense", expense)

                // Tells MainActivity that DataEntry is finished
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }

        // When "Cancel" button is clicked, returns to MainActivity page,
        // tells MainActivity that no result is passed
        btn_cancel.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            setResult(Activity.RESULT_CANCELED, intent)
            finish()
        }
    }
}