package imt3673.ass.groupexpenses

import java.text.DecimalFormatSymbols
import java.util.*

/**
 * Sanitize the name text entries following the specification.
 * See wiki and tests for details.
 */
fun sanitizeName(name: String): String {
    val nameList: List<String>  // List consisting of first and last name
    val fName: String           // First name
    val lName: String?          // Last name
    val fNameList: List<String> // List consisting of parts of first name that are separated by '-'
    val lNameList: List<String> // List consisting of parts of last name that are separated by '-'
    val fNameSanitized: String  // Final sanitized first name
    val lNameSanitized: String  // Final sanitized last name
    val sanitizedName: String   // Final sanitized full name that is returned

    // Trims spaces before and after the name, replaces all spaces in the middle with only one
    var trimmedName = name.trim().replace("\\s+".toRegex(), " ")
    if (trimmedName.isEmpty()) return "No name"

    // Removes symbols other than letters, spaces and hyphens, removed excessive spaces
    val re = Regex("[^A-Za-zæøåÆØÅ -]")
    trimmedName = re.replace(trimmedName, "")
    trimmedName = trimmedName.trim().replace("\\s+".toRegex(), " ")

    // Makes list of first and last name, last name is optional, makes all letters lowercase
    nameList = trimmedName.split(" ")
    fName = nameList[0].toLowerCase(Locale.getDefault())
    lName = nameList.getOrNull(1)?.toLowerCase(Locale.getDefault())

    // Splits first name by hyphen into list, capitalizes first letter in all parts,
    // and combines into one string, if no hyphen just capitalizes first name
    if (fName.contains("-")) {
        fNameList = fName.split("-").toMutableList()

        for (i in 0..fNameList.size-1) {
            if (fNameList[i] != "") {
                fNameList[i] = fNameList[i].first().toUpperCase() + fNameList[i].substring(1)
            }
        }

        fNameSanitized = fNameList.joinToString(separator = "-")
    } else {
        fNameSanitized = fName.first().toUpperCase() + fName.substring(1)
    }

    // If last name, do the same as if statement above and concatenate first and last name
    if (lName != null) {
        if (lName.contains("-")) {
            lNameList = lName.split("-").toMutableList()

            for (i in 0..lNameList.size-1) {
                if (lNameList[i] != "") {
                    lNameList[i] = lNameList[i].first().toUpperCase() + lNameList[i].substring(1)
                }
            }

            lNameSanitized = lNameList.joinToString(separator = "-")
        } else {
            lNameSanitized = lName.first().toUpperCase() + lName.substring(1)
        }

        sanitizedName = fNameSanitized + " " + lNameSanitized
    } else {
        sanitizedName = fNameSanitized
    }

    return sanitizedName
}

/**
 * Utility method for settlement calculations.
 * Takes the Expenses instance, and produces a list of Transactions.
 */
fun calculateSettlement(expenses: Expenses): List<Transaction> {

    // Returns empty list if there are no expenses
    if (expenses.allExpenses().isEmpty()){
        return listOf()
    }

    val owingMap = mutableMapOf<String, Long>()         // Map of people paying below average
    val needingMap = mutableMapOf<String, Long>()       // Map of people paying above average
    val listTransactions = mutableListOf<Transaction>() // List of transactions that is returned

    var amountSum: Long = 0
    val expensesList = expenses.allExpenses()

    // Finds average payment among people
    expensesList.forEach{
        amountSum += it.amount
    }
    val amountAverage = amountSum / expensesList.size

    // Fills maps with how much people owe or need
    expensesList.forEach{
        if(it.amount < amountAverage) {
            owingMap[it.person] = amountAverage - it.amount
        }
        else if(it.amount > amountAverage) {
            needingMap[it.person] = it.amount - amountAverage
        }
    }

    owingMap.forEach { owing ->
        needingMap.forEach { needing ->
            // Payer has to owe and payee has need of amount
            if(owing.value != 0L && needing.value != 0L) {
                // Calculates and adds transactions depending on whether the owing or
                // needing amount is larger
                if(owing.value <= needing.value) {
                    val transaction = Transaction(owing.key, needing.key, owing.value)
                    listTransactions.add(transaction)
                    needingMap[needing.key] = needing.value - owing.value
                    owingMap[owing.key] = 0
                } else {
                    val transaction = Transaction(owing.key, needing.key, needing.value)
                    listTransactions.add(transaction)
                    owingMap[owing.key] = owing.value - needing.value
                    needingMap[needing.key] = 0
                }
            }
        }
    }
    return listTransactions

    /*  FIRST VERSION OF FUNCTION:

        expenses.allExpenses().forEach {
            expensesNumber++
            amountSum += it.amount
        }

        amountAverage = (amountSum / expensesNumber)

        expenses.allExpenses().forEach {
            owesNeedsMap[it.person] = amountAverage - it.amount
        }

        for ((k, v) in owesNeedsMap) {
            if (v > 0) {
                payer = k
                leftToOwe = v
                for ((key, value) in owesNeedsMap) {
                    leftToGet = value * -1
                    if (leftToOwe > 0 && value < 0 && leftToGet > 0) {
                        payee = key
                        if (leftToOwe >= leftToGet) {
                            transactionAmt = leftToGet
                        } else if (leftToGet > leftToOwe) {
                            transactionAmt = leftToOwe
                        }
                        leftToOwe -= transactionAmt
                        leftToGet -= transactionAmt
                        owesNeedsMap[k] = leftToOwe
                        owesNeedsMap[key] = leftToGet
                    }
                }
                if (payer != payee) {
                    transaction = Transaction(payer, payee, transactionAmt)
                    listTransactions.add(transaction)
                }
            }
        }
        return listTransactions
    }
    return listOf()*/
}



/**
 * Converts a given Long amount into a formatted String, with
 * two decimal places. The decimal place separator can be
 * dot or comma, subject to the current locale used.
 */
fun convertAmountToString(amount: Long): String {
    val returnedString: String
    var varAmount = amount

    // The number sign in the returned string
    var sign = ""
    if (varAmount < 0) {
        sign = "-"         // Is '-' if varAmount is negative
        varAmount *= -1    // Turns varAmount positive
    }

    val intPart = (varAmount / 100).toString() // Finds integer part of varAmount
    val decimalPart: String
    val decimal = varAmount % 100              // Finds decimal part

    // Adds additional zero to decimal string if decimal is less than 10
    when (decimal < 10) {
        true -> decimalPart = "0" + decimal.toString()
        false -> decimalPart = decimal.toString()
    }

    // Concatenates sign and full string with local separator character
    val separatorChar: Char = DecimalFormatSymbols.getInstance().decimalSeparator
    returnedString = "${sign}${intPart}${separatorChar}${decimalPart}"

    return returnedString
}

/**
 * Convert from String to Amount. If error, return failed result with
 * appropriate error string.
 */
fun convertStringToAmount(value: String): Result<Long> {
    // Removes spaces, may not be necessary
    var varValue = value.replace("\\s+".toRegex(), "")

    val intPartString: String  // Integer part of string
    val decPartString: String  // Decimal part of string
    val intPart: Long          // Integer part as long
    var decPart: Long          // Decimal part as long

    // Sign is -1 if varValue string is negative (starts with '-'), then all '-' are removed
    var sign = 1
    if (varValue.startsWith("-")) sign = -1
    varValue = varValue.replace("-", "")
    varValue = varValue.trim().replace("\\s+".toRegex(), "")

    if (varValue.isEmpty()) {
        return Result.failure(Throwable("Amount field is empty"))
    }

    // Returns failure if varValue includes characters other than numbers, dots and commas
    if (!varValue.matches(Regex("[0-9]+([,.][0-9]+)?"))) {
        return Result.failure(Throwable("Not a number"))
    }

    // If no decimal part: Multiply long integer part with 100 and sign
    // If decimal part: Split list of parts by separator,
    // return failure if string version of decPart has more than two characters,
    // multiply long decPart by 10 if string version has fewer than two characters
    // so that there are always two decimals,
    // multiply integer part with 100, add decPart, multiply full number with sign
    if (!varValue.contains(".") && !varValue.contains(",")) {
        intPartString = varValue
        intPart = intPartString.toLong()
        return Result.success(intPart * 100 * sign)
    } else {
        val partList = varValue.split(".", ",")
        intPartString = partList[0]
        decPartString = partList[1]

        if (decPartString.length > 2) {
            return Result.failure(Throwable("Too many decimal places"))
        }

        intPart = intPartString.toLong()
        decPart = decPartString.toLong()

        if (decPartString.length < 2) decPart *= 10

        return Result.success(((intPart * 100) + decPart) * sign)
    }
}
