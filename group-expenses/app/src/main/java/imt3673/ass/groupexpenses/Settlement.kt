package imt3673.ass.groupexpenses

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.setPadding
import kotlinx.android.synthetic.main.settlement.*

class Settlement: AppCompatActivity() {

    private var settlementArrList: java.util.ArrayList<Transaction>? = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settlement)

        // Resets settlements table before updating it
        tbl_settlements.removeAllViews()

        // Get settlement list from MainActivity
        settlementArrList = intent.getParcelableArrayListExtra<Transaction>("Settlement")
        updateSettlementTable()

        btn_back.setOnClickListener {
            // Tells MainActivity that Settlement is finished
            val intent = Intent(this, MainActivity::class.java)
            setResult(Activity.RESULT_CANCELED, intent)
            finish()
        }
    }

    private fun updateSettlementTable() {
        var row: TableRow
        var payerText: TextView
        var payeeText: TextView
        var amountText: TextView

        // Header row
        row = TableRow(this)
        row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        payerText = TextView(this)
        payerText.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.toFloat())
        payerText.gravity = Gravity.CENTER
        payerText.setPadding(10)
        payerText.text = getString(R.string.payerText)
        payerText.textSize = 24.toFloat()
        row.addView(payerText)

        payeeText = TextView(this)
        payeeText.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.toFloat())
        payeeText.gravity = Gravity.CENTER
        payeeText.setPadding(10)
        payeeText.text = getString(R.string.payeeText)
        payeeText.textSize = 24.toFloat()
        row.addView(payeeText)

        amountText = TextView(this)
        amountText.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.toFloat())
        amountText.gravity = Gravity.CENTER
        amountText.setPadding(10)
        amountText.text = getString(R.string.amtText)
        amountText.textSize = 24.toFloat()
        row.addView(amountText)

        tbl_settlements.addView(row)

        // Adds row for each settlement
        settlementArrList?.forEach {
            row = TableRow(this)
            row.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )

            payerText = TextView(this)
            payerText.layoutParams = TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT,
                1.toFloat()
            )
            payerText.gravity = Gravity.CENTER
            payerText.setPadding(10)
            payerText.text = it.payer
            payerText.textSize = 24.toFloat()
            row.addView(payerText)

            payeeText = TextView(this)
            payeeText.layoutParams = TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT,
                1.toFloat()
            )
            payeeText.gravity = Gravity.CENTER
            payeeText.setPadding(10)
            payeeText.text = it.payee
            payeeText.textSize = 24.toFloat()
            row.addView(payeeText)

            amountText = TextView(this)
            amountText.layoutParams = TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT,
                1.toFloat()
            )
            amountText.gravity = Gravity.CENTER
            amountText.setPadding(10)
            amountText.text = convertAmountToString(it.amount)
            amountText.textSize = 24.toFloat()
            row.addView(amountText)

            tbl_settlements.addView(row)
        }
    }
}